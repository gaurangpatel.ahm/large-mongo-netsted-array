package com.jdriven.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation.ProjectionOperationBuilder;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.expression.spel.ast.Projection;

import com.jdriven.job.model.Location;
import com.jdriven.job.model.StackOverflow;
import com.jdriven.job.model.TotalLocations;
import com.jdriven.job.repository.StackOverflowRepository;

@SpringBootApplication
public class StackOverflowApp implements CommandLineRunner {

	private static Logger LOG = LoggerFactory.getLogger(StackOverflowApp.class);

	@Autowired
	StackOverflowRepository soRepository;
	
	@Autowired
	MongoTemplate template;

	public static void main(String[] args) {
		SpringApplication.run(StackOverflowApp.class, args);
	}

	@Override
	public void run(String... args) throws IOException, Exception {
//		createRecord();
//		getLocationsSize("f186555a-a219-4c86-9e99-7fd868b14105");
		fetchAllLocationsPaginated("b1f673d5-daba-434f-ae52-403a0e9e14f7");
	}
	
	private void createRecord() {
		StackOverflow saved = soRepository.save(createItem());
		LOG.info("Record Saved {}",saved);
	}
	
	private StackOverflow createItem() {
		StackOverflow item = new StackOverflow();
		item.set_id(UUID.randomUUID());
		LOG.info("UUID: {}",item.get_id());
		item.setParent("Hello Stack overflow");
		item.setLocations(new ArrayList<>());
		for(int i=0;i<5053;i++) {
			item.getLocations().add(new Location("Location "+i,"DIGITAL"));			
		}
		return item;
	}
	
	
	private int getLocationsCount(String id) {
		ProjectionOperation project = Aggregation.project()
				.and("locations")
				.size().as("totalItems");

		TypedAggregation<StackOverflow> agg = Aggregation.newAggregation(StackOverflow.class,
                Aggregation.match(Criteria.where("_id").is(UUID.fromString(id))),
                project);
		
		AggregationResults<TotalLocations> result = template.aggregate(agg, TotalLocations.class);
		LOG.info("{}",result.getMappedResults());
		return result.getMappedResults().get(0).getTotalItems();
	}
	
	private void fetchAllLocationsPaginated(String id) {
		int totalItems = getLocationsCount(id);
		int totalPages = (int) Math.ceil(totalItems/100);
		for(int i=0;i<=totalPages;i++) {
			getPagedResult(id, i);
		}
	}
	
	private void getPagedResult(String id, int pageNo) {
		int offset = pageNo * 100;
		ProjectionOperation project = Aggregation.project()  
				.and("locations").slice(100,offset).as("locations")
				.andInclude("_id")
				.and("parent").as("parent");
		
		TypedAggregation<StackOverflow> agg = Aggregation.newAggregation(StackOverflow.class,
                Aggregation.match(Criteria.where("_id").is(UUID.fromString(id))),
                project);
		
		AggregationResults<StackOverflow> result = template.aggregate(agg, StackOverflow.class);
		LOG.info("Result: {}",result.getMappedResults());
	}
}
