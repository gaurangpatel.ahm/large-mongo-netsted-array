package com.jdriven.job.model;

import java.util.List;
import java.util.UUID;

public class StackOverflow {
	private UUID _id;
	private String parent;
	private List<Location> locations;
	
	public UUID get_id() {
		return _id;
	}
	public void set_id(UUID _id) {
		this._id = _id;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public List<Location> getLocations() {
		return locations;
	}
	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
	@Override
	public String toString() {
		return "StackOverflow [_id=" + _id + ", parent=" + parent + ", locations=" + locations + "]";
	}	
	
}
