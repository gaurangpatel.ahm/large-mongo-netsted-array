package com.jdriven.job.model;

import java.util.UUID;

public class TotalLocations {
	private UUID _id;
	private int totalItems;
	public UUID get_id() {
		return _id;
	}
	public void set_id(UUID _id) {
		this._id = _id;
	}
	public int getTotalItems() {
		return totalItems;
	}
	public void setTotalItems(int totalItems) {
		this.totalItems = totalItems;
	}
	@Override
	public String toString() {
		return "TotalLocations [_id=" + _id + ", totalItems=" + totalItems + "]";
	}
	
}
