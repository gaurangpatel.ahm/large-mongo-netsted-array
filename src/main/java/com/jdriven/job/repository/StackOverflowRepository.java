package com.jdriven.job.repository;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.jdriven.job.model.StackOverflow;

public interface StackOverflowRepository extends MongoRepository<StackOverflow, UUID>  {
	
}
